create table well (
    id integer primary key autoincrement,
    name varchar(32)
);

create table equipment (
    id integer primary key autoincrement,
    name varchar(32),
    well_id integer,
    FOREIGN KEY (well_id) REFERENCES well (id)
);