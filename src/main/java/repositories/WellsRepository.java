package repositories;

import models.Well;

import java.util.List;
import java.util.Optional;

public interface WellsRepository {
    void save(Well well);
    List<Well> findAll();
    Optional<Well> findByName(String name);
}
