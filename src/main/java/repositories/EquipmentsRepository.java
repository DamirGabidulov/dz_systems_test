package repositories;

import models.Equipment;

import java.util.List;

public interface EquipmentsRepository {
    void save(Long wellId);
    List<Equipment> findAll();
    List<Equipment> findAllByWellId(Long wellId);
}
