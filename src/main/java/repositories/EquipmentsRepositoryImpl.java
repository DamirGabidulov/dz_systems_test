package repositories;

import models.Equipment;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public class EquipmentsRepositoryImpl implements EquipmentsRepository {

    //language=SQL
    private static final String SQL_INSERT_EQUIPMENT = "insert into equipment (name, well_id) values (?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL_EQUIPMENTS = "select * from equipment";

    //language=SQL
    private static final String SQL_SELECT_ALL_EQUIPMENTS_BY_WELL_ID = "select * from equipment where well_id = ?";

    private DataSource dataSource;

    public EquipmentsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Equipment> equipmentMapper = resultSet -> {
        try {
            return Equipment.builder()
                    .id(resultSet.getLong("id"))
                    .name(resultSet.getString("name"))
                    .build();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void save(Long wellId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_EQUIPMENT, Statement.RETURN_GENERATED_KEYS)){

            String[] split = UUID.randomUUID().toString().split("-");
            String equipmentName = String.join("", split);

            statement.setString(1, equipmentName);
            statement.setLong(2, wellId);
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("cant insert data");
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Equipment> findAll() {
        List<Equipment> equipments = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_EQUIPMENTS)){
            while (resultSet.next()) {
                equipments.add(equipmentMapper.apply(resultSet));
            }
            return equipments;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Equipment> findAllByWellId(Long wellId) {
        List<Equipment> equipments = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_EQUIPMENTS_BY_WELL_ID)){
            statement.setLong(1, wellId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    equipments.add(equipmentMapper.apply(resultSet));
                }
                return equipments;
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
