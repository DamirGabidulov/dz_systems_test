package repositories;

import models.Well;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class WellsRepositoryImpl implements WellsRepository {

    private final EquipmentsRepository equipmentsRepository;

    //language=SQL
    private static final String SQL_INSERT_WELL = "insert into well (name) values (?)";

    //language=SQL
    private static final String SQL_SELECT_ALL_WELLS = "select * from well";

    //language=SQL
    private static final String SQL_SELECT_BY_WELL_NAME = "select * from well where name = ?";

    private DataSource dataSource;

    public WellsRepositoryImpl(EquipmentsRepository equipmentsRepository, DataSource dataSource) {
        this.equipmentsRepository = equipmentsRepository;
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Well> wellMapper = resultSet -> {
        try {
            return Well.builder()
                    .id(resultSet.getLong("id"))
                    .name(resultSet.getString("name"))
                    .build();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void save(Well well) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_WELL, Statement.RETURN_GENERATED_KEYS)){

            statement.setString(1, well.getName());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1){
                throw new SQLException("cant insert data");
            }

            ResultSet generatedId = statement.getGeneratedKeys();

            if (generatedId.next()){
                well.setId(generatedId.getLong(1));
            } else {
                throw new SQLException("Cant retrieve id");
            }

            System.out.println("rows added");

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Well> findAll() {
        List<Well> wells = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_WELLS)){
            while (resultSet.next()) {
                Well currentWell = wellMapper.apply(resultSet);
                currentWell.setEquipments(equipmentsRepository.findAllByWellId(currentWell.getId()));
                wells.add(currentWell);
            }
            return wells;

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Well> findByName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_WELL_NAME)) {
            statement.setString(1, name);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(wellMapper.apply(resultSet));
                }

                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
