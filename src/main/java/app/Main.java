package app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import repositories.EquipmentsRepository;
import repositories.EquipmentsRepositoryImpl;
import repositories.WellsRepository;
import repositories.WellsRepositoryImpl;
import services.WellsService;
import services.WellsServiceImpl;

import java.util.*;

public class Main {

    private static final String DB_URL = "jdbc:sqlite::resource:test.db";
    public static final String ORG_POSTGRESQL_DRIVER = "org.sqlite.JDBC";

    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(ORG_POSTGRESQL_DRIVER);
        config.setJdbcUrl(DB_URL);
        HikariDataSource dataSource = new HikariDataSource(config);
        EquipmentsRepository equipmentsRepository = new EquipmentsRepositoryImpl(dataSource);
        WellsRepository wellsRepository = new WellsRepositoryImpl(equipmentsRepository, dataSource);
        WellsService wellsService = new WellsServiceImpl(wellsRepository, equipmentsRepository);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            String menu = "Выберите действие: \n" + "Сохранить новую скважину - введите 1 \n"
                    + "Вывод общей информации об оборудовании на скважинах - введите 2 \n"
                    + "Экспорт всех данных в xml файл - введите 3 \n"
                    + "Завершение программы - введите 4";
            System.out.println(menu);
            String choice = scanner.nextLine();
            if (choice.equals("1")) {
                //сохранение
                System.out.println("Введите имя скважины:");
                String wellName = scanner.nextLine();
                System.out.println("Введите кол-во оборудования");
                int equipmentCount = scanner.nextInt();
                wellsService.save(wellName, equipmentCount);
            }

            if (choice.equals("2")) {
                //вывод информации на экран
                System.out.println("Введите имена скважин, разделяя их запятыми без пробелов");
                String wellNames = scanner.nextLine();
                wellsService.findByWellName(wellNames);
            }

            if (choice.equals("3")) {
                //в xml
                System.out.println("Введите имя файла");
                String xmlName = scanner.nextLine();
                wellsService.saveToXmlFile(xmlName);
            }

            if (choice.equals("4")){
                break;
            }
        }
    }
}
