package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@XmlAccessorType(XmlAccessType.FIELD)
public class Well {
    @XmlAttribute
    private Long id;
    @XmlAttribute
    private String name;
    @XmlElement(name = "equipment")
    private List<Equipment> equipments;
}
