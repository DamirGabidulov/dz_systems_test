package services;

import dto.WellToXmlDto;
import models.Equipment;
import models.Well;
import repositories.EquipmentsRepository;
import repositories.WellsRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.List;
import java.util.OptionalInt;

public class WellsServiceImpl implements WellsService {

    private final WellsRepository wellsRepository;
    private final EquipmentsRepository equipmentsRepository;

    public WellsServiceImpl(WellsRepository wellsRepository, EquipmentsRepository equipmentsRepository) {
        this.wellsRepository = wellsRepository;
        this.equipmentsRepository = equipmentsRepository;
    }

    /**
     * Сохрание скважины с оборудованием
     * @param wellName - имя скважины
     * @param equipmentCount - количество оборудования на скважине
     */
    @Override
    public void save(String wellName, int equipmentCount) {
        if (wellsRepository.findByName(wellName).isPresent()){
            throw new IllegalArgumentException("Выберите другое имя");
        }
        Well well = Well.builder()
                .name(wellName)
                .build();
        wellsRepository.save(well);
        for (int i = 1; i <= equipmentCount; i++){
            equipmentsRepository.save(well.getId());
        }
    }

    /**
     * Поиск всех скважин
     * @return список скважин
     */
    @Override
    public List<Well> findAll() {
        return wellsRepository.findAll();
    }

    /**
     * Вывод общей информации об оборудовании на скважинах
     * @param lineOfWellNames - список скважин через запятую
     */
    @Override
    public void findByWellName(String lineOfWellNames) {
        String[] wellNames = lineOfWellNames.split(",");

        for(String wellName : wellNames) {
            List<Well> wells = wellsRepository.findAll();
            OptionalInt first = wells.stream()
                    .filter(well -> well.getName().equals(wellName))
                    .map(Well::getEquipments)
                    .mapToInt(List::size)
                    .findFirst();
            if (first.isPresent()){
                System.out.println("Имя скважины " + wellName + " кол-во оборудования " + first.getAsInt());
            } else {
                System.err.println("Скважина с именем " + wellName + " не существует");
            }
        }
    }

    /**
     * Сохранение данных из базы данных в файл формата xml, также выводит данные в консоль
     * @param fileName - имя файла
     */
    @Override
    public void saveToXmlFile(String fileName) {

        WellToXmlDto wellToXmlDto = WellToXmlDto.builder()
                .wells(findAll())
                .build();

        try {
            JAXBContext context = JAXBContext.newInstance(Well.class, Equipment.class, WellToXmlDto.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Вывод в консоль
            m.marshal(wellToXmlDto, System.out);

            // Печать в файл
            File outFile = new File(fileName + ".xml");
            m.marshal(wellToXmlDto, outFile);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
