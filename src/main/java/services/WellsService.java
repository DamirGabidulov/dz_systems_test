package services;

import models.Well;

import java.util.List;

public interface WellsService {
    void save(String wellName, int equipmentCount);
    List<Well> findAll();
    void findByWellName(String lineOfWellNames);
    void saveToXmlFile(String fileName);
}
